########################################################
#                        patbot                        #
#                     version 1.0.3                    #
#                       quartztz                       #
########################################################

import logging
from subprocess import CalledProcessError
import requests
import random
import os
import re
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters
# from rich import print # temporary, change this please

CAT_URL = "https://api.thecatapi.com/v1/images/search"
LATEX_URL = "https://latex.codecogs.com/png.latex?"
PORT = int(os.environ.get('PORT', 5000))
TOKEN = "5528887828:AAGD540qEiw1CayWZ0c0km0MoQsyXbazxgE"
CUTE_REPLIES = [
  "always glad to help! ૮ ˶ᵔ ᵕ ᵔ˶ ა", 
  "do you really think so? i'm happy :)", 
  "awwww stop that, you're making me blush (⁄ ⁄•⁄ω⁄•⁄ ⁄)⁄", 
  "hehe :33"]
MAD_REPLIES = [
  "ugh, shut up ◔_◔", 
  "whatever..."]

# set up a logger
logging.basicConfig(format = "%(asctime)s - %(name)s - %(levelname)s  - %(message)s", 
                    level = logging.INFO)

logger = logging.getLogger(__name__)

def start(update, context): 
  """reply when called upon for the first time"""
  update.message.reply_text("patpat! you called?")

def kofime(update, context): 
  """if you want better jokes"""
  update.message.reply_text("for better jokes click <a href=\"https://ko-fi.com/quartztz\">here</a>", parse_mode = "HTML")

def help(update, context):
  """offer help when help is requested"""
  update.message.reply_text(
    """alright, fine.
    /start: starts;
    /help: because sometimes, you're just not worthy;
    /patpat: patpat!;
    /coffeeSupremacy: still in beta; 
    /brrou: <s>pussy</s> kitty
    /goodbot: because it's good to say thanks.
    /kofime: shameless plug
    """, parse_mode="HTML")

def goodbot(update, context):
  if update.message.from_user.username == "quartztz" or random.random() < 0.1: 
    update.message.reply_text(random.choice(MAD_REPLIES))
  else: 
    update.message.reply_text(random.choice(CUTE_REPLIES))

def patpat_long(update, context): 
  """patpat!"""
  """TODO: add cool stuff to this; it's basically the only cool method."""
  update.message.reply_text("patpattez vos amis, mesdames, messieurs, et cyborgs.")

def patpat_short(update, context): 
  update.message.reply_text("patpat!")

def coffee(update, context): 
  """when the coffee is supremacist :ooooo"""
  """TODO"""
  update.message.reply_text("hell yeah it is")

def brrou(update, context): 
  """to resurrect an old classic"""
  res = requests.get(CAT_URL)
  neko = res.json()[0]
  update.message.bot.send_photo(update.message.chat.id, neko['url'])

def twss(update, context): 
  """might as well have some fun"""
  update.message.reply_text(f"good one @{update.message.from_user.username}!")

def error(update, context): 
  """because errors will always happen"""
  logger.error(f"welp, looks like an update caused this error: {context.error}")

def latex(update, context): 
  """when the tex is la"""
  try: 
    update.message.reply_text("on it! ;)")
    tex = update.message.text[7:]
    print(f"latex was: {tex}")
    requests.get(LATEX_URL + tex)
    update.message.bot.send_photo("./tex.png")  
  except (CalledProcessError, RuntimeError):
    update.message.reply_text("either you're pulling my leg or that's not good latex. if it's the former that's just mean, but if it's the former sorry i couldn't understand :((")
  
def main(): 
  # start the bot
  updater = Updater(TOKEN, use_context = True)

  dp = updater.dispatcher

  dp.add_handler(CommandHandler("start", start))
  dp.add_handler(CommandHandler("help", help))
  dp.add_handler(CommandHandler("patpat", patpat_short))
  dp.add_handler(CommandHandler("coffeeSupremacy", coffee))
  dp.add_handler(CommandHandler("brrou", brrou))
  dp.add_handler(CommandHandler("goodbot", goodbot))
  dp.add_handler(CommandHandler(["kofime", "fyoukofime"], kofime))
  dp.add_handler(CommandHandler("latex", latex))
  
  # dp.add_handler(MessageHandler(Filters.regex("that's what she said!"), twss))

  dp.add_handler(MessageHandler(Filters.regex(re.compile(r"what she said", re.IGNORECASE)), twss))
  # dp.add_handler(MessageHandler("\latex", latex))


  dp.add_error_handler(error)

  # updater.start_polling()

  updater.start_webhook(listen = '0.0.0.0', 
                        port = int(PORT), 
                        url_path = TOKEN)
  updater.bot.setWebhook('https://hidden-retreat-88900.herokuapp.com/' + TOKEN)

  updater.idle()

if __name__ == '__main__': 
  main()